import re

import pdfplumber
test_list=[]
def extract_values_complete_blood_count(report):
    values = {}
    patterns = {
        "Haemoglobin": r"Haemoglobin (\d+(\.\d+)?) g/dL",
        "RBC Count": r"RBC Count (\d+(\.\d+)?) 10\^12/L",
        "Haematocrit (HCT)": r"Haematocrit \(HCT\) (\d+(\.\d+)?) %",
        "MCV": r"MCV (\d+(\.\d+)?) fl",
        "MCH": r"MCH (\d+(\.\d+)?) pg",
        "MCHC": r"MCHC (\d+(\.\d+)?) g/dL",
        "RDW-CV": r"RDW-CV (\d+(\.\d+)?) %",
        "Platelet Count": r"Platelet Count (\d+(\.\d+)?) 10\^9/L",
        "WBC count, Total": r"WBC count, Total (\d+(\.\d+)?) 10\^9/L",
        "Neutrophils": r"Neutrophils (\d+(\.\d+)?) %",
        "Neutrophil-Absolute Count": r"Neutrophil-Absolute Count (\d+(\.\d+)?) 10\^9/L",
        "Lymphocytes": r"Lymphocytes (\d+(\.\d+)?) %",
        "Lymphocytes-Absolute Count": r"Lymphocytes-Absolute Count (\d+(\.\d+)?) 10\^9/L",
        "Monocytes": r"Monocytes (\d+(\.\d+)?) %",
        "Monocytes-Absolute Count": r"Monocytes-Absolute Count (\d+(\.\d+)?) 10\^9/L",
        "Eosinophils": r"Eosinophils (\d+(\.\d+)?) %",
        "Eosinophils-Absolute Count": r"Eosinophils-Absolute Count (\d+(\.\d+)?) 10\^9/L",
        "Basophils": r"Basophils (\d+(\.\d+)?) %",
        "Basophils-Absolute Count": r"Basophils-Absolute Count (\d+(\.\d+)?) 10\^9/L"
    }

    for parameter, pattern in patterns.items():
        if match := re.search(pattern, report):
            values[parameter] = float(match[1])

    return values
def extract_values_complete_urine_analysis(report):
    values = {}

    patterns = {
        "Colour": r"Colour (.*?) -",
        "Appearance": r"Appearence (.*?) -",
        "Glucose": r"Glucose (.*?) -",
        "Protein": r"Protein (.*?) -",
        "Bilirubin": r"Bilirubin \(Bile\) (.*?) -",
        "Ketone Bodies": r"Ketone Bodies (.*?) -",
        "Specific gravity": r"Specific gravity (.*?) -",
        "Blood": r"Blood (.*?) -",
        "pH": r"Reaction \(pH\) (.*?) -",
        "Nitrites": r"Nitrites (.*?) -",
        "Leukocytes": r"Leukocytes (.*?) -",
        "PUS(WBC) Cells": r"PUS\(WBC\) Cells (.*?) /hpf",
        "Red Blood Cells": r"Red Blood Cells (.*?) /hpf",
        "U.Epithelial Cells": r"U\.Epithelial Cells (.*?) /hpf",
        "Casts": r"Casts (.*?) /hpf",
        "Crystals": r"Crystals (.*?) /hpf",
        "Others": r"Others (.*?) /hpf"
    }

    for parameter, pattern in patterns.items():
        if match := re.search(pattern, report):
            values[parameter] = match[1].strip()

    return values
def extract_values_thyroid_profile(report):
    values = {}

    patterns = {
        "Tri-Iodothyronine Total (TT3)": r"Tri-Iodothyronine Total \(TT3\) (\d+(\.\d+)?) ng/dL",
        "Thyroxine - Total (TT4)": r"Thyroxine - Total \(TT4\) (\d+(\.\d+)?) µg/dL",
        "Thyroid Stimulating Hormone (TSH)": r"Thyroid Stimulating Hormone \(TSH\) (\d+(\.\d+)?) µIU/mL"
    }

    for parameter, pattern in patterns.items():
        if match := re.search(pattern, report):
            values[parameter] = match[1]

    return values
def extract_values_Kidney_function_test(report):
    values = {}

    patterns = {
        "Creatinine": r"Creatinine (\d+(\.\d+)?) mg/dL",
        "Urea": r"Urea (\d+(\.\d+)?) mg/dL",
        "Uric Acid": r"Uric Acid (\d+(\.\d+)?) mg/dL",
        "Sodium (Na)": r"Sodium \(Na\) (\d+(\.\d+)?) mmol/L",
        "Potassium (K)": r"Potassium \(K\) (\d+(\.\d+)?) mmol/L",
        "Chloride(CL)": r"Chloride\(CL\) (\d+(\.\d+)?) mmol/L"
    }

    for parameter, pattern in patterns.items():
        if match := re.search(pattern, report):
            values[parameter] = match[1]

    return values
def extract_values_liver_function_test(report):
    values = {}

    patterns = {
        "Bilirubin Total": r"Bilirubin Total (\d+(\.\d+)?) mg/dL",
        "Bilirubin Direct": r"Bilirubin Direct (\d+(\.\d+)?) mg/dL",
        "Bilurubin Indirect": r"Bilurubin Indirect (\d+(\.\d+)?) mg/dL",
        "Alkaline Phosphatase (ALP)": r"Alkaline Phosphatase \(ALP\) (\d+) U/L",
        "Aspartate Aminotransferase (SGOT)": r"Aspartate Aminotransferase \(SGOT\) (\d+) U/L",
        "Alanine Transaminase (ALT/SGPT)": r"Alanine Transaminase \(ALT/SGPT\) (\d+) U/L",
        "Gamma Glutamyl Transferase (GGT)": r"Gamma Glutamyl Transferase \(GGT\) (\d+) U/L",
        "Protein Total": r"Protein Total (\d+(\.\d+)?) g/dL",
        "Albumin": r"Albumin (\d+(\.\d+)?) g/dL",
        "Globulin": r"Globulin (\d+(\.\d+)?) g/dl",
        "Albumin / Globulin Ratio": r"Albumin / Globulin Ratio (\d+(\.\d+)?)"
    }

    for parameter, pattern in patterns.items():
        if match := re.search(pattern, report):
            values[parameter] = match[1]

    return values
def extract_values_glycosylated(report):
    values = {}

    patterns = {
        "Glycosylated Hemoglobin (GHb/HbA1c)": r"Glycosylated Hemoglobin\(GHb/HbA1c\) (\d+(\.\d+)?) %",
        "Enzymatic Glycosylated Hemoglobin": r"Enzymatic Glycosylated Hemoglobin (\d+(\.\d+)?) mmol/mol",
        "Mean Blood Glucose": r"Mean Blood Glucose (\d+(\.\d+)?) mg/dL"
    }

    for parameter, pattern in patterns.items():
        match = re.search(pattern, report)
        values[parameter] = match[1] if match else None

    return values
def extract_values_lipid_profile(report):
    values = {}

    patterns = {
        "Cholesterol Total": r"Cholesterol Total (\d+(\.\d+)?) mg/dL",
        "Cholesterol HDL": r"Cholesterol HDL (\d+(\.\d+)?) mg / dL",
        "Cholesterol LDL": r"Cholesterol - LDL (\d+(\.\d+)?) mg/dL",
        "Cholesterol VLDL": r"Cholesterol VLDL (\d+(\.\d+)?) mg/dL",
        "Non-HDL cholesterol": r"Non-HDL cholesterol (\d+(\.\d+)?) mg/dL",
        "Triglycerides": r"Triglycerides (\d+(\.\d+)?) mg/dL",
        "Cholesterol Total/Cholesterol HDL Ratio": r"Cholesterol Total/Cholesterol HDL Ratio (\d+(\.\d+)?)",
        "Cholesterol LDL/Cholesterol HDL": r"Cholesterol LDL/Cholesterol HDL (\d+(\.\d+)?)"
    }

    for parameter, pattern in patterns.items():
        match = re.search(pattern, report)
        values[parameter] = match[1] if match else None

    return values
def extract_values_glucose_blood_fasting_and_calcium(report):
    values = {}

    patterns = {
        "25 - Hydroxy Vitamin D- Serum": r"25 - Hydroxy Vitamin D- Serum (\d+(\.\d+)?) ng/mL",
        "Calcium": r"Calcium (\d+(\.\d+)?) mg/dL",
        "Glucose-Blood-Fasting": r"Glucose-Blood-Fasting (\d+(\.\d+)?) mg/dL",
        "Glucose-Blood-Random": r"Glucose - Blood - Random (\d+(\.\d+)?) mg/dL",
    }

    for parameter, pattern in patterns.items():
        match = re.search(pattern, report)
        values[parameter] = match[1] if match else None

    return values
def extract_values_erythrocyte_sedimentation(report):
    values = {}

    if esr_match := re.search(r"Erythrocyte Sedimentation Rate (\d+(\.\d+)?) mm in 1hr", report):
        values["ESR"] = esr_match[1]

    return values
def extract_values_iron_deficiency_profile2(report):
    values = {}

    if iron_match := re.search(r"Iron (\d+(\.\d+)?) µg/dL", report):
        values["Iron"] = iron_match[1]

    if tibc_match := re.search(r"Iron Binding Capacity - Total \(TIBC\) (\d+(\.\d+)?) ug/dL", report):
        values["TIBC"] = tibc_match[1]

    if transferrin_match := re.search(r"Transferrin (\d+(\.\d+)?) mg/dL", report):
        values["Transferrin"] = transferrin_match[1]

    if saturation_match := re.search(r"% Iron Saturation (\d+(\.\d+)?) %", report):
        values["% Iron Saturation"] = saturation_match[1]

    return values

def extract_patient_name(text):
    pattern = r"Patient Name : (.*?)(?= Reg\. No\.)"
    return match[1].strip() if (match := re.search(pattern, text)) else None

def extract_date(text):
    pattern = r"Sample Drawn Date : (\d{2}-\w{3}-\d{4})"
    return match[1] if (match := re.search(pattern, text)) else None
def extract_text_from_pdf(pdf_path):
    # Use the PDF extraction library of your choice
    # Extract the text from the PDF and return it

    # Example using pdfplumber
    

    text = ""
    with pdfplumber.open(pdf_path) as pdf:
        for page in pdf.pages:
            text += page.extract_text()
    # print(text)
    return text

text=extract_text_from_pdf("kacharu.pdf")
test_list.append({"name":extract_patient_name(text)})
test_list.append({"date":extract_date(text)})
# print(test_list)

if "Iron Deficiency Profile-II" in text:
    output= extract_values_iron_deficiency_profile2(text)
    test_list.append({"iron-deficiency-profile-2":output})
    
if "Erythrocyte Sedimentation Rate" in text:
    output= extract_values_erythrocyte_sedimentation(text)
    test_list.append({"erythrocyte-sedimentation-rate":output})

print(test_list)
