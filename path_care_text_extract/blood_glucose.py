import re
import pdfplumber
def extract_patient_name(text):
    pattern = r"Patient Name : (.*?)(?= Reg\. No\.)"
    return match[1].strip() if (match := re.search(pattern, text)) else None

def extract_date(text):
    pattern = r"Sample Drawn Date : (\d{2}-\w{3}-\d{4})"
    return match[1] if (match := re.search(pattern, text)) else None

def extract_blood_glucose(text):
    
    values = {}

    patterns = {
       
        "Glucose-Blood-Fasting": r"Glucose-Blood-Fasting (\d+(\.\d+)?) mg/dL",
        "Glucose-Blood-Random": r"Glucose - Blood - Random (\d+(\.\d+)?) mg/dL",
    }

    for parameter, pattern in patterns.items():
        match = re.search(pattern, text)
        values[parameter] = match[1] if match else None

    return values

def extract_text(pdfPath,pageNumber):
    text = ""
    with pdfplumber.open(pdfPath) as pdf:
        page = pdf.pages[pageNumber]
        text += page.extract_text()
    return text
# Example usage
text = extract_text("kacharu.pdf",0)

patient_name = extract_patient_name(text)
date = extract_date(text)
blood_glucose = extract_blood_glucose(text)

print("Patient Name:", patient_name)
print("Date:", date)
print("Blood Glucose:", blood_glucose)