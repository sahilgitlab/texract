import requests
import pdfplumber


def extract_text_from_pdf(pdf_path, page_number):
    # Use the PDF extraction library of your choice
    # Extract the text from the specified page of the PDF and return it

    # Example using pdfplumber
    text = ""
    with pdfplumber.open(pdf_path) as pdf:
        page = pdf.pages[page_number]
        text += page.extract_text()
    return text


def truncate_text(text, max_tokens):
    tokens = text.split()
    if len(tokens) > max_tokens:
        tokens = tokens[:max_tokens]
        return ' '.join(tokens)
    return text


def get_chatgpt_response(input_text):
    # Make an API call to the ChatGPT API
    # Set up the appropriate headers and parameters
    # Replace 'YOUR_API_KEY' with your actual API key
    # Replace 'gpt-3.5-turbo' with the appropriate ChatGPT model

    api_key = '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'  # Replace with your API key
    model = 'gpt-3.5-turbo'  # Replace with the appropriate ChatGPT model

    headers = {
        'Content-Type': 'application/json',
        'Authorization': f'Bearer {api_key}'
    }

    data = {
        'model': model,
        'messages': [
            {
                'role': 'system',
                'content': 'get all medical test names and their values in key and value pair',
            }
        ],
    }

    # Truncate input_text to fit within the token limit
    max_tokens = 3000

    truncated_text = truncate_text(input_text, max_tokens)
    data['messages'].append({'role': 'user', 'content': truncated_text})
    response = requests.post('https://api.openai.com/v1/chat/completions', headers=headers, json=data)
    print(response.json())
    return response.json()


def extract_key_value_pairs(pdf_path):
    text_pairs = {}

    with pdfplumber.open(pdf_path) as pdf:
        total_pages = len(pdf.pages)
        for page_number in range(total_pages):
            text = extract_text_from_pdf(pdf_path, page_number)
            response = get_chatgpt_response(text)

            if 'error' in response:
                print('Error:', response['error']['message'])
                continue

            generated_text = ''.join(
                choice['message']['content'] for choice in response['choices']
            )

            # Process the generated text to extract key-value pairs
            # Define your own logic based on the structure of the text

            # Example: Assuming the text is structured as "Key: Value"
            lines = generated_text.split('\n')
            for line in lines:
                key_value = line.split(':')
                if len(key_value) == 2:
                    key = key_value[0].strip()
                    value = key_value[1].strip()
                    text_pairs[key] = value

    return text_pairs


# Usage
pdf_path = 'lalita.pdf'
result = extract_key_value_pairs(pdf_path)
# print(result)
